import * as types from '../constants/actionTypes';
import mockData from '../utils/mockData';

const initialState = {
  fetchStatus: false,
  data: mockData,
  error: false,
};

const comments = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_COMMENT:
      return {
        ...state,
          fetchStatus: true,
          error: false,
      };
    case types.ADD_COMMENT_SUCCESS:
      return {
        ...state,
          fetchStatus: false,
          data: [...state.data, action.payload]
      };
    case types.ADD_COMMENT_FAILED:
      return {
        ...state,
          fetchStatus: false,
          error: action.payload
      };

    default:
      return state;
  }
};

export default comments;
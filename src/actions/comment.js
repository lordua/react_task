import * as types from '../constants/actionTypes';
import config from '../constants/config';

let id = 3;

export function addComment (data) {

  return dispatch => {
    dispatch( request() );
    postComment(data)
      .then(response => {
        dispatch( success({...response.data, id: id}) );
        ++id;
      })
      .catch(errors => dispatch( failure(errors) ));
  };

  function postComment(data) {
    let errors = [];
    if(data.comment.length > config.MAX_INPUT_LENGTH) {
      errors.push({ message: "This message to long" })
    }
    if(errors.length) {
      Promise.reject({ success: false, data: { errors }});
    }
    return Promise.resolve({ success: true, data});
  }

  function request() { return {type: types.ADD_COMMENT}}
  function success(data) { return {type: types.ADD_COMMENT_SUCCESS, payload: data}}
  function failure(errors) { return {type: types.ADD_COMMENT_FAILED, payload: errors}}
}
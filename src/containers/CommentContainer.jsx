import React, { Component } from 'react';
import { connect } from 'react-redux';
import { array, func } from 'prop-types';
import config from '../constants/config';
import Comment from '../components/Comment.jsx';
import CommentForm from '../components/CommentForm.jsx';
import Error from '../components/Error.jsx';
import avatarImg from '../images/Eric-cartman.png';
import { addComment } from '../actions/comment';

class CommentContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: {
        name: 'Eric Cartman',
        avatar: avatarImg,
      },
      formValid: true,
    };
  }

  onSubmit = () => {
    const { addComment } = this.props;
    const { currentUser } = this.state;

    if (this.element.value.length > config.MAX_INPUT_LENGTH) {
      this.setState({
        formValid: false
      });
      return;
    }

    if (!this.element.value.trim().length) {
      return;
    }

    this.setState({
      formValid: true
    });
    const comment = {
      ...currentUser,
      comment: this.element.value,
    };

    setTimeout(() => addComment(comment), config.COMMENT_TIMEOUT);
    this.element.value = '';
  };

  onKeyUp = (e) => {
    const { addComment } = this.props;
    const { currentUser } = this.state;

    if (this.element.value.length > config.MAX_INPUT_LENGTH) {
      this.setState({formValid: false});
      return;
    }

    if (!this.element.value.trim().length) {
      return;
    }

    this.setState({
      formValid: true
    });
    if(e.keyCode === config.KEY_CODE) {
      const comment = {
        ...currentUser,
          comment: this.element.value,
      };

    setTimeout(() => addComment(comment), config.COMMENT_TIMEOUT);
    this.element.value = '';
    }
  };

  render() {
    const { comments } = this.props;
    const { formValid, currentUser } = this.state;
    const hasError = comments.error && comments.error.length;
    const errors = hasError && comments.error.join(' . ');
    const toggleError = hasError ? <Error className='error' message={errors} /> : null;

    return (
      <div className="comments-form col-md-6 offset-md-3">
        <p className="comment-message">
          Comments
        </p>
        <CommentForm
          avatar={currentUser.avatar}
          inputElement={element => this.element = element}
          onKeyUp={this.onKeyUp}
          onSubmit={this.onSubmit}
        />
        {

          !formValid ?
        <Error className='error' message={ config.ERROR_MESSAGE } /> : toggleError
        }
        {
          comments.map((comment) =>
            <Comment
              avatarImg={comment.avatar}
              name={comment.name}
              comment={comment.comment}
              key={comment.id}
            />
          )
        }
      </div>
    );
  }
}

CommentContainer.propTypes = {
  comments: array,
  addComment: func.isRequired,
};

CommentContainer.defaultprops = {
  comments: [],
};

const mapStateToProps = state => ({comments: state.comments.comments.data});
const mapDispatchToProps = dispatch => ({ addComment: (data) => { dispatch( addComment(data)) } });

export default connect(mapStateToProps, mapDispatchToProps)(CommentContainer);

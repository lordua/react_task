import React from 'react';
import { func, string } from 'prop-types';
import UserAvatar from './UserAvatar.jsx';

const CommentForm = ({ avatar, inputElement, onKeyUp, onSubmit }) => (
  <div className="form">
    <UserAvatar avatarImg={avatar}/>
    <input
      type="text"
      placeholder="Add a comment..."
      className="col-md-9"
      ref={inputElement}
      onKeyUp={onKeyUp}
    />
    <button
      type="button"
      className="btn btn-primary"
      onClick={onSubmit}
    >
      Submit
    </button>
  </div>
);

CommentForm.propTypes = {
  avatar: string.isRequired,
  inputElement: func.isRequired,
  onKeyUp: func.isRequired,
  onSubmit: func.isRequired
};

export default CommentForm;

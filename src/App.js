import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store/store';
import CommentsContainer from './containers/CommentContainer';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App row">
          <CommentsContainer />
        </div>
      </Provider>
    );
  }
}

export default App;

import kennyImg from '../images/kenny.png';
import buttersImg from '../images/Butters.png';

const mockData = [
  {
    id: 1,
    name: 'Kenny Mccormick',
    avatar: kennyImg,
    comment: 'Mph rmph rm rmph rm Mphrm rmph rm rm rm mph Mph rm'
  },
  {
    id: 2,
    name: 'Butters Scotch',
    avatar: buttersImg,
    comment: 'Hi guys!'
  }
];

export default mockData;
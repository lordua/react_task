import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import { withRouter } from 'react-router'
import thunk from 'redux-thunk';


import comments from '../reducers/index';

let initialState = {};

const Store = createStore(
  combineReducers({
    routing: routerReducer,
    comments
  }),
  initialState,
  compose(
    applyMiddleware(routerMiddleware(withRouter), thunk)
  )
);


export default Store;


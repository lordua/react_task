import React from 'react';
import { string } from 'prop-types';

const UserAvatar = ({avatarImg}) => (
  <img src={avatarImg} alt="avatar"/>
);

UserAvatar.propTypes = {
  avatarImg: string.isRequired
};

export default UserAvatar;

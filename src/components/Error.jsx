import React from 'react';

const Error = ({ className, message }) => (
  <div className={className}>{message}</div>
);

export default Error;

import React from 'react';
import UserAvatar from './UserAvatar.jsx';
import { string } from 'prop-types';

const Comment = ({ name, comment, avatarImg }) => (
  <div className="comments">
    <div className="comment">
      <UserAvatar avatarImg={avatarImg}/>
    <div className="text-comment">
      <p className="user-name">
        {name}
      </p>
      <p>
        {comment}
      </p>
    </div>
    </div>
  </div>
);

Comment.propTypes = {
  name: string.isRequired,
  comment: string.isRequired,
  avatarImg: string.isRequired,
};

export default Comment;
